<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->genre = $data->genre;

$filter = $book->filterByGenres();

$num_rows = $filter->rowCount();

if ($num_rows > 0) {
    $books = array();

    while ($result = $filter->fetch(PDO::FETCH_ASSOC)) {
        extract($result);
        $bookItem = array(
            'id' => $id,
            'cover_pic' => $cover_pic,
            'genre' => $genre,
            'user_id' => $user_id,
        );
        array_push($books, $bookItem);

    }
    echo json_encode($books);
} else {
    echo json_encode(array(
        'message' => 'No books found',
    ));
}
