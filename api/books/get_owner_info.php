<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->user_id = $data->user_id;

$get_owner = $book->getOwner();

if ($get_owner) {
    $result = $get_owner->fetch(PDO::FETCH_ASSOC);
    extract($result);

    $username = array(
        'username' => $username,
    );

    echo json_encode($username);
} else {
    echo json_encode(array(
        'message' => 'Something went wrong',
    ));
}
