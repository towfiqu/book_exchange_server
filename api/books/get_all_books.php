<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

// calling query methods to get the data
$get_books = $book->getAllBooks();

// getting the number of rows that a sql query returns
$num_rows = $get_books->rowCount();

if ($num_rows > 0) {
    $books = array();
    $books['data'] = array();

    while ($row = $get_books->fetch(PDO::FETCH_ASSOC)) {
        extract($row); // this will extract data from query.Things like row['id'], row['email'] etc

        $book_item = array(
            'id' => $id,
            'cover_pic' => $cover_pic,
            'user_id' => $user_id,
        );

        array_push($books['data'], $book_item);

    }
    echo json_encode($books);

} else {
    echo json_encode(array(
        'message' => 'No books found',
    ));
}
