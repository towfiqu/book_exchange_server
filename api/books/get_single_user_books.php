<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->user_id = $data->user_id;

$get_single_user_books = $book->getSingleUserBooks();

$num_rows = $get_single_user_books->rowCount();

// get all the books for single user

if ($num_rows > 0) {
    $book_array = array();
    $book_array['data'] = array();

    while ($res = $get_single_user_books->fetch(PDO::FETCH_ASSOC)) {
        extract($res);
        $book_item = array(
            'id' => $id,
            'title' => $title,
            'author' => $author,
            'genre' => $genre,
            'cover_pic' => $cover_pic,
            'collection' => $collection,
        );

        array_push($book_array['data'], $book_item);
    }

    echo json_encode($book_array);
} else {
    echo json_encode(array(
        'message' => 'No books found',
    ));
}
