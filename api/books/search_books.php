<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->title = $data->title;

$search = $book->searchBooks();

$num_rows = $search->rowCount();

if ($num_rows > 0) {
    $books = array();

    while ($result = $search->fetch(PDO::FETCH_ASSOC)) {
        extract($result);
        $bookItem = array(
            'id' => $id,
            'title' => $title,
            'author' => $author,
            'cover_pic' => $cover_pic,
        );
        array_push($books, $bookItem);

        
    }
    echo json_encode($books);
} else {
    echo json_encode(array(
        'message' => 'No books found',
    ));
}
