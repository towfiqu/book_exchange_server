<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$genres = $book->getAllGenres();

if ($genres) {
    $all_genres = array();
    while ($result = $genres->fetch(PDO::FETCH_NAMED)) {
        extract($result);

        array_push($all_genres, $genre);
    }

    echo json_encode($all_genres);

} else {
    echo json_encode(array(
        'message' => 'No genres',
    ));
}
