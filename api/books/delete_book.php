<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->id = $data->book_id;

$delete_book = $book->deleteBook();

if ($delete_book) {
    echo json_encode(array(
        'message' => 'Book Deleted',
    ));
} else {
    echo json_encode(array(
        'message' => 'Something Went Wrong while deleting book!',
    ));
}
