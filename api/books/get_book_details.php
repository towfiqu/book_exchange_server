<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate book (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->id = $data->book_id;

$get_book = $book->getBookDetails(); // sending query request

if ($get_book) {
    $bookDetails = $get_book->fetch(PDO::FETCH_ASSOC);
    extract($bookDetails); // kinda like destructuring in JS

    $book_data = array(
        'id' => $id,
        'title' => $title,
        'author' => $author,
        'genre' => $genre,
        'book_details' => $book_details,
        'cover_pic' => $cover_pic,
        'user_id' => $user_id,
    );

    echo json_encode($book_data);

} else {
    echo json_encode(array(
        'message' => 'Something Went Wrong while getting the book!',
    ));
}
