<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->user_id = $data->user_id;

$get_books_in_col = $book->getBooksCountInLib();

$res = $get_books_in_col->fetch(PDO::FETCH_NUM);

$books_count_in_col = array(
    'books_count' => $res[0],
);

echo json_encode($books_count_in_col);
