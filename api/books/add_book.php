<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Book.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$book = new Book($db_connect);

$data = json_decode(file_get_contents('php://input'));

$book->title = $data->title;
$book->author = $data->author;
$book->genre = $data->genre;
$book->details = $data->details;
$book->cover_pic = $data->cover_pic;
$book->collection = $data->collection;
$book->want_to_read = $data->want_to_read;
$book->user_id = $data->user_id;

$add_book = $book->addBook();

if ($add_book) {
    echo json_encode(array(
        'message' => 'Book Added',
    ));
} else {
    echo json_encode(array(
        'message' => 'Something Went Wrong while adding book!',
    ));
}
