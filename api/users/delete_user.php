<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/User.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$user = new User($db_connect);

$user->id = isset($_GET['id']) ? $_GET['id'] : die();

if ($user->deleteUser()) {
    echo json_encode(array(
        'message' => 'User Deleted',
    ));
} else {
    echo json_encode(array(
        'message' => 'Something Went Wrong!',
    ));
}
