<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/User.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$user = new User($db_connect);

$data = json_decode(file_get_contents('php://input'));

$user->id = $data->user_id;

$get_user = $user->getUserById();

$result = $get_user->fetch(PDO::FETCH_ASSOC);
extract($result);

$user_data = array(
    'id' => $id,
    'username' => $username,
    'email' => $email,
    'password' => $password,
    'profile_pic' => $profile_pic,

);

echo json_encode($user_data);
