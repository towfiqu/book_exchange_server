<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/User.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$user = new User($db_connect);

// calling query methods to get the data
$get_users = $user->getUsers();



// getting the number of rows that a sql query returns
$num_rows = $get_users->rowCount();

  
if ($num_rows) {
    $users = array();
    $users['data'] = array();

    while ($row = $get_users->fetch(PDO::FETCH_ASSOC)) {
        extract($row); // this will extract data from query.Things like row['id'], row['email'] etc
        

        $user_item = array(
            'id' => $id,
            'username' => $username,
            'email' => $email,
        );

        array_push($users['data'], $user_item);

    }
    echo json_encode($users);

} else {
    echo json_encode(array(
        'message' => 'No users found',
    ));
}
