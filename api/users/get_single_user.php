<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/User.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$user = new User($db_connect);

// Getting the id from url params and assigning it to user->id
$data = json_decode(file_get_contents('php://input'));

$user->email = $data->email;
$user->password = $data->password;

$get_single_user = $user->getSingleUser();
$num_rows = $get_single_user->rowCount();

if ($num_rows > 0) {
    $data = $get_single_user->fetch(PDO::FETCH_ASSOC);
    extract($data);

    $user_data = array(
        'id' => $id,
        'username' => $username,
        'email' => $email,
        'password' => $password,
        'profile_pic' => $profile_pic,

    );

    // user's books count logic
    $user->id = $user_data['id'];
    $booksCount = $user->getSingleUserBooksCount();

    $res = $booksCount->fetch(PDO::FETCH_NUM);

    $user_books_count = array(
        'user_books' => $res[0],
    );

    // combining user's data and user's books count;
    $combined = array_merge($user_data, $user_books_count);

    echo json_encode($combined);

} else {
    echo json_encode(array(
        'message' => 'User not found',
    ));
}
