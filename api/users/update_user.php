<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/User.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$user = new User($db_connect);

$data = json_decode(file_get_contents('php://input'));

$user->id = $data->id;

$user->username = $data->username;
$user->email = $data->email;
$user->password = $data->password;
$user->profile_pic = $data->profile_pic;

if ($user->updateUser()) {
    echo json_encode(array(
        'message' => 'User Updated',
    ));
} else {
    echo json_encode(array(
        'message' => 'Something Went Wrong!',
    ));
}
