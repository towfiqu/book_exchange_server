<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/User.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

// Instantiate user (passing db connection)
$user = new User($db_connect);

$data = json_decode(file_get_contents('php://input'));

$user->username = $data->username;
$user->email = $data->email;
$user->password = $data->password;

$get_user = $user->getSingleUser();
$num_rows = $get_user->rowCount();

if ($num_rows > 0) {
    echo json_encode(array(
        'message' => 'User already exists',
    ));
} else {

    $create_user = $user->createUser();

    if ($create_user) {
        echo json_encode(array(
            'message' => 'User Created',
        ));
    } else {
        echo json_encode(array(
            'message' => 'Something Went Wrong!',
        ));
    }

}
