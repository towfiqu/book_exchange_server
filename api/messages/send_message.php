<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Message.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

$msg = new Message($db_connect);

$data = json_decode(file_get_contents('php://input'));

$msg->msg = $data->msg;
$msg->username = $data->username;
$msg->email = $data->email;
$msg->profile_pic = $data->profile_pic;
$msg->sender_id = $data->sender_id;
$msg->receiver_id = $data->receiver_id;

$create_msg = $msg->sendMessage();

if ($create_msg) {
    echo json_encode(array(
        'response' => 'Message sent',
    ));
} else {
    echo json_encode(array(
        'response' => 'Something went wrong while sending message',
    ));

}
