<?php
//Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Message.php';

// Instantiate database & connect
$database = new Database();
$db_connect = $database->connect();

$msg = new Message($db_connect);

$data = json_decode(file_get_contents('php://input'));

$msg->sender_id = $data->user_id;
$msg->receiver_id = $data->user_id;

$get_messages = $msg->getAllMessages();

if ($get_messages) {

    $message_array = array();

    while ($result = $get_messages->fetch(PDO::FETCH_ASSOC)) {
        extract($result);
        $msg_item = array(
            'id' => $id,
            'messages' => $messages,
            'username' => $username,
            'email' => $email,
            'profile_pic' => $profile_pic,
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
        );

        array_push($message_array, $msg_item);

    }

    echo json_encode($message_array);
}
