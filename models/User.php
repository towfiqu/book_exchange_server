<?php
class User
{
    private $conn;

    public $id;
    public $username;
    public $email;
    public $password;
    public $profile_pic;

    // constructor function
    public function __construct($db_connect)
    {
        $this->conn = $db_connect;

    }

    // methods
    public function getUsers()
    {
        $query = 'SELECT * from users';

        // each query needs to go through database connection
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function getSingleUser()
    {
        $query = 'SELECT * from users WHERE email= :email AND password= :password';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);

        $stmt->execute();

        return $stmt;

    }

    public function getUserById()
    {
        $query = 'SELECT * from users WHERE id= :user_id';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':user_id', $this->id);

        $stmt->execute();

        return $stmt;

    }

    public function getSingleUserBooksCount()
    {
        $query = 'SELECT COUNT(*) from books WHERE user_id=:user_id AND collection="yes"';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':user_id', $this->id);
        $stmt->execute();
        return $stmt;
    }

    public function createUser()
    {

        $query = 'INSERT INTO users SET username= :username, email= :email, password= :password';

        $stmt = $this->conn->prepare($query);

        if ($this->username != null && $this->email != null && $this->password != null) {
            $stmt->bindParam(':username', $this->username);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);

        }

        if ($stmt->execute()) {
            return true;
        } else {
            printf('Error: %s. \n', $stmt->error);
            return false;
        }

    }
    public function updateUser()
    {
        $query = 'UPDATE users SET username= :username, email= :email, password= :password, profile_pic = :profile_pic WHERE id = :id';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id', $this->id);

        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':profile_pic', $this->profile_pic);

        if ($stmt->execute()) {
            return true;
        } else {
            printf('Error: %s. \n', $stmt->error);
            return false;
        }

    }
    public function deleteUser()
    {
        $query = 'DELETE from users WHERE id = :id';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id', $this->id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf('Error: %s. \n', $stmt->error);
            return false;
        }

    }
}
