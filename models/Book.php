<?php
class Book
{

    // private fields
    private $conn;

    // public fields

    public $id;
    public $title;
    public $author;
    public $genre;
    public $details;
    public $cover_pic;
    public $collection;
    public $want_to_read;
    public $user_id;

    // constructor
    public function __construct($db_connect)
    {
        $this->conn = $db_connect;
    }

    //methods

    public function getAllGenres()
    {
        $query = 'SELECT DISTINCT genre from books WHERE collection="yes"';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;

    }

    public function filterByGenres()
    {
        $query = 'SELECT * from books WHERE genre=:genre AND collection="yes"';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':genre', $this->genre);
        $stmt->execute();
        return $stmt;

    }

    public function searchBooks()
    {
        $query = 'SELECT * from books WHERE title=:title AND collection="yes"';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':title', $this->title);
        $stmt->execute();
        return $stmt;
    }

    public function getAllBooks()
    {
        $query = 'SELECT id, cover_pic, user_id from books WHERE collection="yes"';

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function getOwner()
    {
        $query = 'SELECT username from users WHERE id = :user_id';
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute();
        return $stmt;
    }

    public function getBookDetails()
    {
        $query = 'SELECT * from books WHERE id=:book_id';

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':book_id', $this->id);
        $stmt->execute();
        return $stmt;
    }

    public function addBook()
    {
        $query = "INSERT INTO books SET title = :title, author = :author, genre = :genre, book_details = :details, cover_pic = :cover_pic, collection = :collection, want_to_read = :want_to_read, user_id = :user_id";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':genre', $this->genre);
        $stmt->bindParam(':details', $this->details);
        $stmt->bindParam(':cover_pic', $this->cover_pic);
        $stmt->bindParam(':collection', $this->collection);
        $stmt->bindParam(':want_to_read', $this->want_to_read);
        $stmt->bindParam(':user_id', $this->user_id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf('Error: %s. \n', $stmt->error);
            return false;
        }
    }

    public function deleteBook()
    {
        $query = 'DELETE from books WHERE id = :book_id';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':book_id', $this->id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf('Error: %s. \n', $stmt->error);
            return false;
        }
    }

    public function getBooksCountInLib()
    {
        $query = 'SELECT COUNT(*) from books WHERE user_id = :user_id';
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute();
        return $stmt;
    }

    public function getSingleUserBooks()
    {
        $query = 'SELECT * from books WHERE user_id = :user_id';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute();
        return $stmt;
    }

}
