<?php
class Message
{
    private $conn;

    public $msg;
    public $sender_id;
    public $receiver_id;
    public $username;
    public $email;
    public $profile_pic;

    public function __construct($db_connect)
    {
        $this->conn = $db_connect;

    }

    public function sendMessage()
    {
        $query = 'INSERT INTO messages SET messages= :msg, username=:username, email=:email, profile_pic=:profile_pic, sender_id= :sender_id, receiver_id= :receiver_id';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':msg', $this->msg);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':profile_pic', $this->profile_pic);
        $stmt->bindParam(':sender_id', $this->sender_id);
        $stmt->bindParam(':receiver_id', $this->receiver_id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf('Error: %s. \n', $stmt->error);
            return false;
        }

    }

    public function getAllMessages()
    {
        $query = 'SELECT * from messages WHERE sender_id= :user_id OR receiver_id= :user_id';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':user_id', $this->sender_id);
        $stmt->bindParam(':user_id', $this->receiver_id);
        $stmt->execute();
        return $stmt;
    }

    public function getIndiChats()
    {
        $query = 'SELECT * from messages WHERE sender_id= :sender_id AND receiver_id= :receiver_id OR sender_id=:receiver_id AND receiver_id= :sender_id';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':sender_id', $this->sender_id);
        $stmt->bindParam(':receiver_id', $this->receiver_id);
        $stmt->execute();
        return $stmt;

    }

}
